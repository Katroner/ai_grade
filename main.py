import os
from os.path import exists

import tensorflow as tf
from rich import print as printc
from rich.console import Console

from src import validate_is_number
from utils import DataGenerator, NeuralNetwork


def final_grade(predictions: list) -> float:
    final_score = 0

    grades = predictions[::2]
    percentages = predictions[1::2]

    for i, j in zip(grades, percentages):
        final_score += i * (j / 100)

    final_score = round(final_score, 2)

    return final_score


def get_message(final_score) -> int:
    if 4.5 < final_score <= 5:
        return 4

    elif 4 <= final_score < 4.5:
        return 3

    elif 3 <= final_score < 4:
        return 2

    else:
        return 1


def automate(predict: list) -> None:
    model = tf.keras.models.load_model('network/')

    result = model.predict([predict]).flatten()  # type: ignore
    real_grade = final_grade(predict)
    real_score = get_message(real_grade)
    result[0] = round(result[0], 2)
    result[1] = round(result[1], 2)
    print(f"\nPrediction: {result} Real: {real_grade} {real_score}")


def test_model() -> None:
    try:
        predict = [4.5, 90, 3.8, 4, 4.6, 6, 0.0, 0, 0.0, 0, 0.0, 0]
        automate(predict)

        predict = [4.5, 90, 3.8, 2, 4.6, 2, 2.5, 3, 3.0, 3, 0.0, 0]
        automate(predict)

        predict = [2.1, 85, 4.8, 4, 5.0, 6, 1.2, 1, 2.8, 2, 4.1, 2]
        automate(predict)

    except Exception as e:
        print(f"[!] Error type: {type(e)}")
        print(f"Error: {e}")


def main() -> None:
    os.system("cls")
    console = Console()
    data = None

    try:
        if not exists("data/grades.csv"):
            data = DataGenerator()
            printc(
                "\n[green][!] Please enter the length of the data for training the model:\n")

            data.data_length = validate_is_number(input())

            if not data.creating_data():
                raise Exception(data.error)

        if not exists("network/"):
            network = NeuralNetwork()
            printc(
                "\n[blue][!] Please enter the number of epochs to train the model")
            network.epochs = validate_is_number(input())

            if not network.making_neural_network():
                raise Exception(network.error)

        test_model()

    except KeyboardInterrupt:
        printc("[red]\n[!] Stopping...")

    except Exception:
        console.print_exception(show_locals=True)

    finally:
        if data != None:
            data = None


if __name__ == '__main__':
    main()
