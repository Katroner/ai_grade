import os

from rich import print as printc


def clear_and_print(message: str) -> None:
    os.system("cls")
    printc(f"[red]{message}")


def validate_is_number(number: str) -> int:
    message = "[!] You must enter a number"

    while not number.isdigit():
        clear_and_print(message)
        number = input("\n[-] Enter a valid number: ")

    return int(number)
