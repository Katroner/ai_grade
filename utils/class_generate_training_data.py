import csv
import os
import random

from rich import print as printc
from tqdm import tqdm


class DataGenerator():
    # Constructor
    def __init__(self) -> None:
        self.__data_length: int = 0

        self.__grades_intervals: list[tuple[float, float]] = [
            (0.0, 2.9), (3.0, 3.9),
            (4.0, 4.4), (4.5, 5.0)]

        self.__grades: list[float] = []
        self.__percentages_grades: list[int] = []
        self.__total_grade: float = 0
        self.__point: int = 0
        self.__error: Exception = Exception("")

    # Properties

    def __set_data_length(self, val: int) -> None:
        self.__data_length = val

    def __get_error(self) -> Exception:
        return self.__error

    data_length = property(fset=__set_data_length)
    error = property(fget=__get_error)

    # Private methods
    def __validate_data(self) -> bool:
        if self.__data_length <= 0:
            printc(
                f"\n[red][!] The length of the data must be positive and greater than 0")
            return False

        return True

    def __generate_grades(self, values: tuple[float, float], grades_quantity: int) -> list[float]:
        grades: list[float] = []

        for _ in range(grades_quantity):
            grades.append(round(random.uniform(values[0], values[1]), 2))

        if len(grades) != 6:
            for _ in range(6 - len(grades)):
                grades.append(0.0)

        return grades

    def __generate_percentages(self, quantity_percentages: int) -> list[int]:
        total_percentage: int = 0
        percentages_grades: list[int] = []

        while total_percentage != 100:
            for i in range(quantity_percentages):
                percent = random.randint(5, 80)
                total_percentage += percent
                percentages_grades.append(percent)

            if quantity_percentages == 6:
                if total_percentage != 100 or len(percentages_grades) != 6:
                    total_percentage = 0
                    percentages_grades = []

            else:
                if total_percentage != 100:
                    total_percentage = 0
                    percentages_grades = []

        if len(percentages_grades) != 6:
            for _ in range(6 - len(percentages_grades)):
                percentages_grades.append(0)

        return percentages_grades

    def __final_grade(self) -> float:
        total_grade: float = 0

        for i, j in zip(self.__grades, self.__percentages_grades):
            total_grade += i * (j / 100)

        total_grade = round(total_grade, 2)

        return total_grade

    def __get_score(self) -> int:
        if 4.5 < self.__total_grade <= 5:
            return 4

        elif 4 <= self.__total_grade < 4.5:
            return 3

        elif 3 <= self.__total_grade < 4:
            return 2

        else:
            return 1

    def __save_data(self, data: list) -> bool:
        try:
            with open("data/grades.csv", "a", newline="") as f:
                writer = csv.writer(f)
                writer.writerow(data)

            return True

        except Exception as e:
            self.__error = e
            return False

    # Public methods
    def creating_data(self) -> bool:
        if not self.__validate_data():
            return False

        os.system("cls")

        printc("\n[blue][!] Generating data...\n")

        val: int = random.randint(0, 3)
        counter: int = random.randint(2, 6)

        for _ in tqdm(range(self.__data_length)):
            self.__grades = self.__generate_grades(
                self.__grades_intervals[val], counter)
            self.__percentages_grades = self.__generate_percentages(counter)
            self.__total_grade = self.__final_grade()
            self.__point = self.__get_score()

            data = []

            for i, j in enumerate(self.__grades):
                data.append(self.__grades[i])
                data.append(self.__percentages_grades[i])

            data.append(self.__total_grade)
            data.append(self.__point)

            try:
                if not self.__save_data(data):
                    return False

            except Exception as e:
                self.__error = e

            val = random.randint(0, 3)
            counter = random.randint(2, 6)

        printc("\n[green][!] Data genereded!")

        return True
