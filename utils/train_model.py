from os.path import exists
from time import sleep

import pandas as pd
import tensorflow as tf
from rich import print as printc


class NeuralNetwork():
    # Constructor
    def __init__(self) -> None:
        self.__data_csv: pd.DataFrame
        self.__x_train: pd.DataFrame
        self.__y_train: pd.DataFrame
        self.__x_test: pd.DataFrame
        self.__x_test: pd.DataFrame
        self.__model: tf.keras.Sequential
        self.__number_epochs: int = 0
        self.__error: Exception = Exception()

    # Properties
    def __set_epochs(self, val: int):
        self.__number_epochs = val

    def __get_error(self) -> Exception:
        return self.__error

    error = property(fget=__get_error)
    epochs = property(fset=__set_epochs)

    # Private methods
    def __validate(self) -> bool:
        if self.__number_epochs <= 0:
            printc(
                "\n[red][!] The number of epochs has to be positive and bigger than 0")
            return False

        if not exists("data/grades.csv"):
            printc("\n[red][!] The file which contains the data does not exist")
            return False

        return True

    def __set_data(self) -> None:
        try:
            self.__data_csv = pd.read_csv("data/grades.csv", header=None)
            limit = len(self.__data_csv) * 0.8
            limit = int(limit)

            self.__x_train = self.__data_csv.iloc[:limit, :12]
            self.__y_train = self.__data_csv.iloc[:limit, 12:]
            self.__x_test = self.__data_csv.iloc[limit:, :12]
            self.__y_test = self.__data_csv.iloc[limit:, 12:]

        except FileNotFoundError as e:
            self.__error = e

        except Exception as e:
            self.__error = e

    def __make_model(self) -> None:
        hidden1 = tf.keras.layers.Dense(
            48,
            input_shape=[12],
            activation="relu")

        hidden2 = tf.keras.layers.Dense(96, activation="relu")
        hidden3 = tf.keras.layers.Dense(48, activation="relu")
        output = tf.keras.layers.Dense(2)

        self.__model = tf.keras.Sequential([hidden1, hidden2, hidden3, output])

    def __compile_train(self) -> None:
        self.__model.compile(
            optimizer=tf.keras.optimizers.Adam(0.01),
            loss="mean_absolute_error"
        )

        # history = self.__model.fit(
        #     self.__x_train,
        #     self.__y_train,
        #     epochs=1000,
        #     verbose=1  # type: ignore
        # )

        self.__model.fit(
            self.__x_train,
            self.__y_train,
            epochs=self.__number_epochs,
            verbose=1  # type: ignore
        )

        self.__model.evaluate(
            self.__x_test,
            self.__y_test,
            verbose=1)  # type: ignore

    def __test_model(self) -> None:
        predictions = self.__model.predict(self.__x_test).flatten()

        a = predictions[::2]
        b = predictions[1::2]

        y_pred = pd.DataFrame({12: a, 13: b}, columns=[12, 13])

        y_pred[12] = y_pred[12].apply(lambda x: round(x, 2))
        y_pred[13] = y_pred[13].apply(lambda x: round(x, 2))

        counter: int = 0

        print("Prediction                   Real")

        for (index1, row1), (index2, row2) in zip(y_pred.iterrows(), self.__y_test.iterrows()):
            with open("prove.txt", "a") as f:
                f.write(
                    f"\nGrade: {row1[12]} Score: {row1[13]}     |     Grade: {row2[12]} Score: {row2[13]}")

            # if counter == 20:
            #     break

            counter += 1

    def __save_model(self) -> None:
        self.__model.save('network/')

    # Public methods
    def making_neural_network(self) -> bool:
        try:
            if not self.__validate():
                return False

            printc("\n[blue][+] Getting data...")
            sleep(1)

            self.__set_data()

            printc("\n[blue][+] Making model...\n")
            sleep(1)
            self.__make_model()

            printc("\n[yellow][+] Trainning network...")
            sleep(1)
            self.__compile_train()

            printc("\n[green][+] Testing...")
            sleep(1)
            self.__test_model()

            printc("\n[yellow][+] Saving model...")
            sleep(1)
            self.__save_model()

            print("\n[+] The model is trained and saved")
            sleep(1)

            return True

        except Exception as e:
            self.__error = e
            return False
